# Angularjs test #

#### Test Description
* Create a product landing page which show all products from a array of products, when user click on each product then open a dialog modal with features as bebow:
    * Informations will be display on dialog modal : title, desc, img
    * Optionally specify the height and width as either a px or % value
    * Overlay the rest of the screen
    * Close by either clicking an X in the corner or outside the dialog modal

#### Technical requirement
* Create a angular service with a public api **getProducts** which return a array of products (mock data, don't need to get from database) wtih properties : title, desc, img
* Create a custom directive with isolated scope and restrict is "E" to display a product item, for instance **<product-item></product-item>**
* Create a custom directive with isolated scope and restrict is "AE" for dialog modal, for instance as below :

> <modal-dialog show='modalShown' width='750px' height='60%'>
>  <p>Product Desc Goes here<p>
> </modal-dialog>

* Notes : use **ng-transclude** indicates where your modal content will go. And also note the use of **ng-style**, which will translate a javascript object into an inline style
* Optionally with responsive layout display
* Optionally use animation effect when show/hide dialog modal
* Optionally use Unit Tests, for example as Jasmine or others.

#### Instructions
Fork this repository. Create a new branch to do your work. When done create a pull request back to studio60-vietnam as a new branch.
If there are any questions, feel free to record any assumptions made.
